package ma.tp.projet.test;

import java.util.ArrayList;
import java.util.List;

import ma.tp.projet.classes.Employe;
import ma.tp.projet.classes.Etudiant;
import ma.tp.projet.classes.Professeur;

public class Main {

	public static void main(String[] args) {
		
		List<Etudiant> etudiants = new ArrayList<Etudiant>();
		etudiants.add(new Etudiant(1, "Marie","Clire","EO025"));
		etudiants.add(new Etudiant(2, "Thomas","Doe","EO027"));
		
		List<Employe> employes = new ArrayList<Employe>();		
		employes.add(new Employe(1, "Jean", "Baton", 1500.00));
		employes.add(new Employe(2, "Roland", "Garros", 1000.00));
		
		List<Professeur> professeurs = new ArrayList<Professeur>();
		professeurs.add(new Professeur(3, "Kevin", "Frey", 5700.00,"Java/JEE"));
		professeurs.add(new Professeur(4, "Mathieu", "Dupont", 5000.00,"Mathématiques"));
		
		etudiants.forEach(System.out::println);
		employes.forEach(System.out::println);
		professeurs.forEach(System.out::println);


	}

}
