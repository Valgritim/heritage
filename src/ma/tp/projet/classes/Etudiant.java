package ma.tp.projet.classes;

public class Etudiant extends Personne{

	private String cne;
	
	public Etudiant(int id, String firstName, String lastName,String cne) {
		super(id, firstName, lastName);
		
		this.cne = cne;
	}

	public String getCne() {
		return cne;
	}

	public void setCne(String cne) {
		this.cne = cne;
	}

	@Override
	public String toString() {
		return "Je suis" + " " + firstName + " " + lastName + ", mon CNE est " + cne;
	}

	
	
	
	
	
	
}
