package ma.tp.projet.classes;

public class Employe extends Personne {
	
	protected Double salary;

	public Employe(int id, String firstName, String lastName, Double salary) {
		super(id, firstName, lastName);
		this.salary = salary;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {		
		return "Je suis" + " " + firstName + " " + lastName + ", mon salaire est " + salary + " euros";
	}
	
	

}
