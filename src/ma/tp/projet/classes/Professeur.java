package ma.tp.projet.classes;

public class Professeur extends Employe {
	
	private String specialty;

	public Professeur(int id, String firstName, String lastName,Double salary, String specialty) {
		super(id, firstName, lastName, salary);
		
		this.specialty = specialty;	
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	@Override
	public String toString() {
		return "Je suis" + " " + firstName + " " + lastName + ", mon salaire est " + salary + " euros, ma spécialité est: " + specialty;
	}
	
	

}
